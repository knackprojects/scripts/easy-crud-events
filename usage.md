```javascript
// EXAMPLE USE
LazyLoad(['https://gl.githack.com/knackprojects/scripts/easy-crud-events/raw/master/script.js'], function(){
    let cn = new Knack.fn.dbConnection(API_KEY);
    
    var table = new cn.table('object_ID');
    
});
```

```javascript
// EXAMPLE USE GET ALL RECORDS
LazyLoad(['https://gl.githack.com/knackprojects/scripts/easy-crud-events/raw/master/script.js'], function(){
    let cn = new Knack.fn.dbConnection(API_KEY);
    
    var table = new cn.table('object_ID');
        // get all records on the table 
        table.records();
});
```

```javascript
// EXAMPLE USE GET ALL RECORDS WITH OPTIONS 
LazyLoad(['https://gl.githack.com/knackprojects/scripts/easy-crud-events/raw/master/script.js'], function(){
    let cn = new Knack.fn.dbConnection(API_KEY);
    
    var table = new cn.table('object_ID');
        // get all records on the table 
        table.records({
            onComplete : function(response){ .. }
        });
    
});
```

```javascript
// OTHER EXAMPLES
LazyLoad(['https://gl.githack.com/knackprojects/scripts/easy-crud-events/raw/master/script.js'], function(){
    let cn = new Knack.fn.dbConnection(API_KEY);
    
    var table = new cn.table('object_ID');
            // get all records on the table 
            table.records({ .. });
            
            // get record ID 
            table.record({
                onComplete : function(response){ .. },
                // all callbacks 
            }, record_ID);
            
            table.create({
                field_ID : value, 
                field_ID : value, .. 
            }, function(response){
                // callback after create
            });
            
            table.create({
                field_ID : value, 
                field_ID : value, .. 
            }, {
                onComplete : function(response){ .. },
                // other options implement 
            });
    
});
```

OPTIONS 
```javascript
{
    onBeforeSend : null,
    onAfterSend : null,
    formatResponse : null,
    find : null,
    onDone : null,
    onError : null, // no implemented
    onComplete : null,
}
```