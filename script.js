Knack=Knack||{}; Knack.fn=Knack.fn||{}; Knack.data=Knack.data||{};

Knack.fn.dbConnection = function(API_KEY){

  let defaults = {
    onBeforeSend : null,
    onAfterSend : null,
    formatResponse : null,
    find : null,
    onDone : null,
    onError : null,
    onComplete : null,
  }

  let _headers = {
    "Content-Type": "application/json", "x-knack-new-builder": true,
    Authorization: Knack.getUserToken(), "X-Knack-Application-Id": Knack.application_id,
    "X-Knack-REST-API-Key": "builder", "X-Requested-With": "XMLHttpRequest", "X-Knack-REST-API-Key" : API_KEY
  };

  let _setting = {};

  let methods = {};

  methods._call = function(args, callback, filters, id, page = 1){
    let filters_ = typeof filters==="object"?`?rows_per_page=1000&page=${page}&filters=`+encodeURIComponent(JSON.stringify(filters)):`?rows_per_page=1000&page=${page}`;
    Knack.$.ajax({
      url : `${args.url}${filters_}`,
      type : args.type||'GET', 
      headers : _headers, data : JSON.stringify(args.data||{}), success : function(data){ typeof callback === 'function' ? callback(data) : false; }
    });
  }

  /**
   * GET 
   */

  methods.get = function(object_id, callback, filters, id = '', page = 1){
    console.log(typeof filters);
    console.log(object_id);
    let filters_ = typeof filters==="object"?`?rows_per_page=1000&page=${page}&filters=`+encodeURIComponent(JSON.stringify(filters)):`?rows_per_page=1000&page=${page}`;
    Knack.$.ajax({
      url : `https://us-api.knack.com/v1/objects/${object_id}/records/${id}${filters_}`,
      type : 'GET', 
      headers : _headers, success : function(data){ typeof callback === 'function' ? callback(data) : false; }
    });
  };

  /**
   * POST 
   */
  methods.post = function(object_id, data, callback, filters, id, page = 1){
    let filters_ = typeof filters==="object"?`?rows_per_page=1000&page=${page}&filters=`+encodeURIComponent(JSON.stringify(filters)):`?rows_per_page=1000&page=${page}`;
    Knack.$.ajax({
      url : `https://us-east-1-builder-write.knack.com/v1/objects/${object_id}/records/${filters_}`,
      type : 'POST', 
      headers : _headers, data : JSON.stringify(data), success : function(data){ typeof callback === 'function' ? callback(data) : false; }
    });
  };

  /**
   * PUT 
   */
  methods.put = function(object_id, data, callback, filters, id, page = 1){
    let filters_ = typeof filters==="object"?`?rows_per_page=1000&page=${page}&filters=`+encodeURIComponent(JSON.stringify(filters)):`?rows_per_page=1000&page=${page}`;
    Knack.$.ajax({
      url : `https://us-api.knack.com/v1/objects/${object_id}/records/${id}${filters_}`,
      type : 'PUT', 
      headers : _headers, data : JSON.stringify(data), success : function(data){ typeof callback === 'function' ? callback(data) : false; }
    });
  };

  /**
   |
   | Response funcitons
   |
   |
   */


  return {
    table : function(id){
      let object_id = id;
      let fn = methods;
      let _filter = null;
      let _dfSettings = {
        filters : false,
      };
      let data = {};
      let setts = Object.assign({}, defaults);

      var responsefn = function(dt, opt){

        typeof opt.iterateRecord === 'function' ? opt.iterateRecord(dt) : '';
        typeof opt.onComplete === 'function' ? opt.onComplete(dt) : '';
        typeof opt.onDone === 'function' ? opt.onDone(dt) : '';


        console.log('data %o', [dt, opt]);

        let data_obj = dt;
        return {
          data : data_obj,
          opt  : opt
        }
      }

      return {
        records : function(options){
          let opt = Object.assign({}, setts, _dfSettings, options);
          typeof opt.onBeforeSend==='function'&&opt.onBeforeSend();
          return fn.get(object_id, function(response){
            return responsefn(response, opt);
          }, opt.filters);
          typeof opt.onAfterSend === 'function' ? opt.onAfterSend() : '';
        },
        record : function(options, id){
          let opt = Object.assign({}, setts, _dfSettings, options);
          typeof opt.onBeforeSend==='function'&&opt.onBeforeSend();
          return fn.get(object_id, function(response){
            return responsefn(response, opt);
          }, opt.filters, `${id}/`);
          typeof opt.onAfterSend === 'function' ? opt.onAfterSend() : '';
        },
        create : function(data, options){
          let opt = {};
          if(typeof options === 'object')
            opt = Object.assign({}, setts, _dfSettings, options);
          return fn.post(object_id, data, function(response){
            typeof options==='function'?options(response):responsefn(response, opt);
          }, opt.filters);

        }, 
        update : function(options, id){
          let opt = {};
          if(typeof options === 'object')
            opt = Object.assign({}, setts, _dfSettings, options);
          return fn.put(object_id, opt.data, function(response){
            typeof options==='function'?options(response):responsefn(response, opt);
          }, opt.filters, id);
        },
        exec : function(options, callback){
          let df_args = {
            url : '',
            type : 'GET'
          };

          opt = Object.assign({}, setts, _dfSettings, df_args, options);
          
          fn._call(opt, callback, opt.filters);
        },
        collection : function(options){
          let opt = Object.assign({}, setts, _dfSettings, options);
          typeof opt.onBeforeSend==='function'&&opt.onBeforeSend();

          let records = [];

          let first = fn.get(object_id, function(response){
            return responsefn(response, opt);
          }, opt.filters, '', '1');

          records = records.concat(first);

          let total_pages = (first&&first.data&&first.data.total_pages)||1;

          for(var i=2;i<=total_pages;i++){
            var page = fn.get(object_id, function(response){
              return responsefn(response, opt);
            }, opt.filters, '', i);

            records = records.concat(page);
          };

          typeof opt.onAfterSend === 'function' ? opt.onAfterSend() : '';

          return typeof options==='function'?options(records):responsefn(records, opt);

        },

      };
    }
  }


  

};

